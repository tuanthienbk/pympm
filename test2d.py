#!/usr/bin/python
from mesh import *
from mpm2d import *


gravity = [0,-9.8]

mesh = read_off('circ3.off')
mesh.v[:,1] = mesh.v[:,1] - 1 
v0 = np.zeros((mesh.nf,2))
grid1 = Grid(0.2, 0.2)
obj1 = mpmObject(grid1, mesh, 100., gravity, 1.5e4, 0.2, 0.95, v0)

mesh2 = read_off('circ3.off')
mesh2.v[:,1] = mesh2.v[:,1]  + 1.2
mesh2.v[:,0] = mesh2.v[:,0]  + 0.2
v2 = np.zeros((mesh2.nf,2))
grid2 = Grid(0.2, 0.2)
obj2 = mpmObject(grid2, mesh2, 100., gravity, 1.5e5, 0.2, 0.95, v2)

objLst = []
collisionPairs = []
objLst.append(obj1)
objLst.append(obj2)
collisionPairs.append((obj1,obj2))
animationLoop(objLst, collisionPairs, 5e-3)
