import numpy as np

def linearSpline(u,h):
    B = np.zeros(2)
    Bx = np.zeros(2)
    B[0] = 1-u
    B[1] = u
    Bx[0] = -1/h
    Bx[1] = 1/h
    return B,Bx

def cubicSpline(u,h):
    B = np.zeros(4)
    Bx = np.zeros(4)
    B[0] = (1/6.0)*(1-u)**3
    B[1] = (1/6.0)*(4-6*u**2+3*u**3)
    B[2] = (1/6.0)*(1+3*u+3*u**2-3*u**3)
    B[3] = (1/6.0)*(u**3)
    Bx[0] = -(1/(2.0*h))*(1-u)**2 
    Bx[1] = (1/(6.0*h))*(-12*u+9*u**2)
    Bx[2] = (1/(6.0*h))*(3+6*u-9*u**2)
    Bx[3] = (1/(2.0*h))*(u**2)
    return B,Bx