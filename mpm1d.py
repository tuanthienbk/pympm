#!/usr/bin/python

import numpy as np

# computational grid
E = 1000
L = 1.0
nodes = np.array([0,L])
elements = np.array([0,1])
nbElems = elements.shape[0]
nbNodes = nodes.shape[0]

# Material points
xp = 0.5*L  # position
Mp = 1      # mass
Vp = 1      # volume
vp = 0.1    # velocity
s = 0.      # stress
q = Mp*vp   # momentum

# time loop
time = 1.0
dt = 0.01
t = 0

ta = []
va = []
xa = []

while(t < time):
    # shape functions and derivatives
    N1 = 1 - np.abs( xp - nodes[0])/L
    N2 = 1 - np.abs( xp - nodes[1])/L
    dN1 = -1/L
    dN2 = 1/L
    # particle mass and momentum
    m1 = N1*Mp
    m2 = N2*Mp
    mv1 = N1*q
    mv2 = N2*q
    mv1 = 0     # boundary condtion
    # internal force
    fint1 = - Vp*s*dN1
    fint2 = - Vp*s*dN2
    f1 = fint1
    f2 = fint2
    #update nodal momentum
    f1 = 0 # boundary condition 
    
    mv1 = mv1 + f1 * dt
    mv2 = mv2 + f2 * dt
    # update particle velocity and position
    vp = vp + dt*( N1*f1/m1 + N2*f2/m2 )
    xp = xp + dt*( N1*mv1/m1 + N2*mv2/m2 )
    q = Mp * vp
    # momentum
    v1 = N1*Mp*vp/m1
    v2 = N2*Mp*vp/m2
    v1 = 0                  # boundary condition
    Lp = dN1*v1 + dN2*v2    # gradient velocity
    dEps = dt*Lp            # straint increment
    s = s + E*dEps          # stress update
    # store time, velocity for ploting
    ta.append(t)
    va.append(vp)
    xa.append(xp)
    # advance time step
    t = t + dt

v_exact = 0.1*np.cos((np.sqrt(E)/L)*np.array(ta))
                         
import matplotlib.pyplot as plt
plt.plot(ta,va,'-bo')
plt.plot(ta,v_exact,'-rx')
plt.show()

