import numpy as np
from scipy.sparse import coo_matrix,find
from matplotlib.tri import Triangulation
import matplotlib.pyplot as plt

class Mesh:
    def __init__(self, vertices,faces):
        self.v = vertices
        self.f = faces
        self.nv = vertices.shape[0]
        self.nf = faces.shape[0]
        self.ngon = faces.shape[1]
        row = np.zeros(self.nf*self.ngon,dtype = int)
        col = np.zeros(self.nf*self.ngon,dtype = int)
        data = np.zeros(self.nf*self.ngon,dtype = int)
        for i,f in enumerate(faces):
            for j in range(self.ngon):
                row[i*self.ngon + j] = f[j]
                col[i*self.ngon + j] = f[(j+1)%self.ngon]
                data[i*self.ngon + j] = i+1 # caution ! face index is from 1 not 0
        self.e = coo_matrix((data,(row,col)), shape=(self.nv,self.nv)).tocsr()
        self.isboundaryv = np.zeros(self.nv,dtype=bool)
        self.isboundaryf = np.zeros(self.nf,dtype=bool)
        for i,f in enumerate(faces):
            for j in range(self.ngon):
                v1 = f[j]
                v2 = f[(j+1)%self.ngon]
                if (self.e[v2,v1] == 0):  
                    self.e[v2,v1] = -1 # outter face has index -1
                    self.isboundaryf[i] = 1
                    self.isboundaryv[v1] = 1;
                    self.isboundaryv[v2] = 1;
        self.valence = np.zeros(self.nv,dtype = int)
        for i,f in enumerate(vertices):
            [_,j,_] = find(self.e[i,:])
            self.valence[i] = len(j) 
        self.eov = [s for s,x in enumerate(self.valence) if ((not x==4) and self.isboundaryv[s])] 
    def oneringv(self,v):
        [_,j,_] = find(self.e[v,:])
        return j
    def oneringf(self,v):
        a = self.e[v,:][self.e[v,:] > 0]
        b = self.e[:,v][self.e[:,v] > 0]
        return np.union1d(np.array(a).reshape(-1), np.array(b).reshape(-1))
    def getArea(self, i):
        v10 = self.v[self.f[i,1]] - self.v[self.f[i,0]] 
        v20 = self.v[self.f[i,2]] - self.v[self.f[i,0]]
        return 0.5*np.linalg.norm(np.cross(v10, v20))
    def refine(self):
        if (self.ngon == 4):
            [i,j,_] = find(self.e)
            [_,idx,_] = find(i<j)
            ne = len(idx)
            row = np.zeros(ne,dtype = int)
            col = np.zeros(ne,dtype = int)
            data = np.zeros(ne,dtype = int)
            for k in range(ne):
                row[k] = i[idx[k]]
                col[k] = j[idx[k]]
                data[k] = k
            edge_idx = coo_matrix((data,(row,col)), shape=(self.nv,self.nv)).tocsr()
            dim = len(self.v[0,:])
            v = np.zeros([self.nv+self.nf+ne,dim],dtype=float)
            f = np.zeros([4*self.nf,4],dtype=int)
            for pp in range(self.nf):
                v1 = self.f[pp,0]
                v2 = self.f[pp,1]
                v3 = self.f[pp,2]
                v4 = self.f[pp,3]
                v5 = (edge_idx[v1,v2] if (v1 < v2) else edge_idx[v2,v1]) + self.nv
                v6 = (edge_idx[v2,v3] if (v2 < v3) else edge_idx[v3,v2]) + self.nv
                v7 = (edge_idx[v3,v4] if (v3 < v4) else edge_idx[v4,v3]) + self.nv
                v8 = (edge_idx[v4,v1] if (v4 < v1) else edge_idx[v1,v4]) + self.nv
                v[v1,:] = self.v[v1,:]
                v[v2,:] = self.v[v2,:]
                v[v3,:] = self.v[v3,:]
                v[v4,:] = self.v[v4,:]
                v[v5,:] = (self.v[v1,:] + self.v[v2,:])/2
                v[v6,:] = (self.v[v2,:] + self.v[v3,:])/2
                v[v7,:] = (self.v[v3,:] + self.v[v4,:])/2
                v[v8,:] = (self.v[v4,:] + self.v[v1,:])/2
                v9 = self.nv+ne+pp 
                v[v9,:] = (self.v[v1,:] + self.v[v2,:] + self.v[v3,:] + self.v[v4,:])/4
                f[4*pp] = np.array([v1,v5,v9,v8])
                f[4*pp+1] = np.array([v5,v2,v6,v9])
                f[4*pp+2] = np.array([v8,v9,v7,v4])
                f[4*pp+3] = np.array([v9,v6,v3,v7])
            return Mesh(v,f)

def read_off(filename):
    fl = open(filename,'r')
    if 'OFF' != fl.readline().strip():
        print 'Not a valid OFF header'
    nv, nf, ne = tuple([int(s) for s in fl.readline().strip().split(' ')])
    
    tmp = [float(s) for s in fl.readline().strip().split(' ')]
   
    ncoord = len(tmp)
    v = np.empty([nv,ncoord],dtype = float)
    v[0] = tmp
    for i in range(nv-1):
        v[i+1] =  [float(s) for s in fl.readline().strip().split(' ')]

    tmp = [int(s) for s in fl.readline().strip().split(' ')][1:]
    ngon = len(tmp)
    f = np.empty([nf,ngon],dtype = int)
    f[0] = tmp
    for i in range(nf-1):
        f[i+1] = [int(s) for s in fl.readline().strip().split(' ')][1:]
    fl.close()
    return Mesh(v, f)

def draw(mesh,c):
    tf = mesh.f   
    if (mesh.ngon == 4):
        tf = np.r_[mesh.f[:,0:3],mesh.f[:,[0,2,3]]]
    tri = Triangulation(mesh.v[:,0],mesh.v[:,1],tf)
    plt.triplot(tri, color=c)
    plt.axis('equal')
    return plt.hold(True)