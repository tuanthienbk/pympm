import numpy as np
from scipy.linalg import polar
import matplotlib.pyplot as plt
from spline import *

class Grid:
    def __init__(self, hx, hy):
        self.hx = hx
        self.hy = hy
        self.mass = {}
        self.massdiff = {}
        self.vp = {}
        self.vf = {}
        self.force = {}
        self.du = {}
        self.df = {}
        
    def getCoord(self, x, y):
        xx = x/self.hx
        yy = y/self.hy
        i = int(np.floor(xx))
        j = int(np.floor(yy))
        u = xx - i
        v = yy - j
        return i,j,u,v
      
    def reset(self):
        self.mass = {}
        self.massdiff = {}
        self.vp = {}
        self.vf = {}
        self.force = {}
        self.du = {}
        self.df = {}
        
    def updateVelocitiesExplicit(self, dt):
        for i, mm in enumerate(self.vp):
            self.vp[mm] = self.vp[mm]/self.mass[mm]
            self.vf[mm] = self.vp[mm] + (dt/self.mass[mm])*self.force[mm]        #explicit method
            #collision hack
            if (mm[1] < -2/self.hy):
                self.vf[mm] = 0*self.vf[mm]

class Particle:
    def __init__(self, pos, vel, vol0, mass):
        self.pos = pos
        self.vel = vel
        self.vol0 = vol0
        self.vol = 0
        self.mass = mass        
        self.sigma = np.zeros((2,2))
        self.F = np.identity(2)
                
    def getForce(self,wx,wy):
        f = np.zeros(2)
        f[0] = - self.vol*(self.sigma[0,0]*wx + self.sigma[0,1]*wy) 
        f[1] = - self.vol*(self.sigma[1,0]*wx + self.sigma[1,1]*wy) 
        return f
    
    def updateDeformationGradient(self,dt,L):
        self.F = (np.identity(2) + dt*L).dot(self.F)
    
    def updateVelocity(self, dt, dv, alpha):
        self.vel = self.alpha*self.vel + dv
        
    def updatePosition(self, dt):
        self.pos = self.pos + dt*self.vel
        
    def collideFloor(self, height):
        if (self.pos[1] <= height):
            self.vel = -self.vel
    
    def updateSigma(self, mu, lamda):
        '''
        Neo-Hookean model
        '''
#         self.sigma = (mu/J)*(self.F.dot(self.F.T) - np.identity(2)) + (lamda*np.log(J)/J)*np.identity(2)
        '''
        Co-rotated model
        '''
        R, S = polar(self.F)
        J = S[0,0]*S[1,1]
        self.vol = J*self.vol0
        self.sigma = (mu*(self.F - R).dot(self.F) + lamda*(J - 1)*J*np.identity(2))/J
             
                        
class mpmObject:
    '''
    @summary initialize rest pose
    @param mesh: a triangular mesh (see mesh.py)
    @param rho: density
    @param E: Young modulus
    @param nu: Poisson's ratio
    @param alpha: weight for PIC and FLIP
    @param vel0: initial velocities  
    '''
    def __init__(self, grid, mesh, rho, gravity, E, nu, alpha, vel0):
        self.grid = grid
        self.gravity = gravity
        self.mu = E/(2*(1+nu))
        self.lamda = E*nu/((1-2*nu)*(1+nu)) # can't use the name lambda, reserved word ?
        self.alpha = alpha
        self.vel = vel0
        self.nP = mesh.nf
        self.mass = np.ones(self.nP)
        self.vol0 = np.zeros(self.nP)
        self.pos = np.zeros((self.nP,2))
#         self.J = np.zeros(self.nP)
#         self.sigma = [None]*self.nP
        self.F = [None]*self.nP
        for i in range(self.nP):
            self.vol0[i] = mesh.getArea(i)
            self.mass[i] = self.vol0[i]*rho
            self.pos[i,:] = sum(mesh.v[mesh.f[i,:],0:2])/3
#             self.sigma[i] = np.zeros((2,2))
            self.F[i] = np.identity(2)

    '''
    @summary compute new state for grid nodes from particles information
    '''
    def stepNode(self,dt):
        # reset mass, velocities and forces at grid nodes to zero
        self.grid.reset()
        #particle to node    
        for k in range(self.nP):
            '''
            Co-rotated model
            '''
#             R, S = polar(self.F[k])
#             J = S[0,0]*S[1,1]
#             sigma = self.vol0[k]*(self.mu*(self.F[k] - R).dot(self.F[k]) + self.lamda*(J - 1)*J*np.identity(2)) 
            '''
            StVK model
            First Piola-Kirchhof: P = F[2*mu*E + lambda*tr(E)*I], E = 0.5*(F^T*F - I)
            Cauchy stress: sigma = 1/J * P * F^T
            '''
    #         b = F[k].dot(F[k].T)
    #         sigma[k] = (mu/J)*(b.dot(b - np.identity(2))) + (0.5*lamda/J)*(b[0,0] + b[1,1] - 2)*F[k].T            
            '''
            Neo-Hookean model
            '''
            J = np.linalg.det(self.F[k])            
            sigma = self.vol0[k]*((self.mu)*(self.F[k].dot(self.F[k].T) - np.identity(2)) + (self.lamda*np.log(J))*np.identity(2))

            x = self.pos[k][0]
            y = self.pos[k][1]
            i,j,u,v = self.grid.getCoord(x,y)            
    #         [Bx,dBx] = linearSpline(x, i, hx)
    #         [By,dBy] = linearSpline(y, j, hy)
            [Bx,dBx] = cubicSpline(u, self.grid.hx)
            [By,dBy] = cubicSpline(v, self.grid.hy)
            for i1 in range(0,4):
                for i2 in range(0,4):
                    idx = (i-1+i1,j-1+i2)
                    if (idx in self.grid.mass):
                        self.grid.mass[idx] = self.grid.mass[idx] + Bx[i1]*By[i2]*self.mass[k]
                        self.grid.massdiff[idx][0] = self.grid.massdiff[idx][0] + dBx[i1]*By[i2]*self.mass[k]
                        self.grid.massdiff[idx][1] = self.grid.massdiff[idx][1] + Bx[i1]*dBy[i2]*self.mass[k]
                        # this is actually momentum, later we divide by the mass to obtain velocities
                        self.grid.vp[idx] = self.grid.vp[idx] + Bx[i1]*By[i2]*self.mass[k]*self.vel[k,:] 
                        self.grid.force[idx][0] = self.grid.force[idx][0] - (sigma[0,0]*dBx[i1]*By[i2] + sigma[0,1]*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*self.mass[k]*self.gravity[0]
                        self.grid.force[idx][1] = self.grid.force[idx][1] - (sigma[1,0]*dBx[i1]*By[i2] + sigma[1,1]*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*self.mass[k]*self.gravity[1]
                    else:
                        self.grid.mass[idx] = Bx[i1]*By[i2]*self.mass[k]
                        self.grid.massdiff[idx] = np.zeros(2)
                        self.grid.massdiff[idx][0] = dBx[i1]*By[i2]*self.mass[k]
                        self.grid.massdiff[idx][1] = Bx[i1]*dBy[i2]*self.mass[k]
                        # this is actually momentum, later we divide by the mass to obtain velocities
                        self.grid.vp[idx] = Bx[i1]*By[i2]*self.mass[k]*self.vel[k,:]
                        self.grid.force[idx] = np.zeros(2) 
                        self.grid.force[idx][0] = - (sigma[0,0]*dBx[i1]*By[i2] + sigma[0,1]*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*self.mass[k]*self.gravity[0]
                        self.grid.force[idx][1] = - (sigma[1,0]*dBx[i1]*By[i2] + sigma[1,1]*Bx[i1]*dBy[i2]) + Bx[i1]*By[i2]*self.mass[k]*self.gravity[1]
                    
        # update velocities using explicit Euler
        self.grid.updateVelocitiesExplicit(dt)
    
    '''
    @summary compute new state for particles from grid nodes
    '''    
    def stepParticle(self,dt):    
        #node to particle
        dv = np.zeros_like(self.vel)
        L = [None]*self.nP
        for k in range(self.nP):
            L[k] = np.zeros((2,2))
            #update particle vp
            x = self.pos[k][0]
            y = self.pos[k][1]
            i,j,u,v = self.grid.getCoord(x,y)            
    #         [Bx,dBx] = linearSpline(x, i, hx)
    #         [By,dBy] = linearSpline(y, j, hy)
            [Bx,dBx] = cubicSpline(u, self.grid.hx)
            [By,dBy] = cubicSpline(v, self.grid.hy)
            
            for i1 in range(0,4):
                for i2 in range(0,4):
                    idx = (i-1+i1,j-1+i2)
                    if (self.grid.mass[idx] > 1e-12):
                        dv[k,:] = dv[k,:] + (self.grid.vf[idx]  - self.alpha*self.grid.vp[idx])*Bx[i1]*By[i2]
                        L[k][:,0] = L[k][:,0] + (dBx[i1]*By[i2])*self.grid.vf[idx]    
                        L[k][:,1] = L[k][:,1] + (Bx[i1]*dBy[i2])*self.grid.vf[idx]      
        
        for k in range(self.nP):
            self.vel[k] = self.alpha*self.vel[k] + dv[k]
            self.pos[k] = self.pos[k] + dt*self.vel[k]
            #collision hack
            if (self.pos[k,1] <= -2):
                self.vel[k] = -self.vel[k]
            
            self.F[k] = (np.identity(2) + dt*L[k]).dot(self.F[k])
  
    
    def forceDiff(self, dt, u, wx, wy, F, R, S, J):
        dF = dt*np.array([ [wx*u[0], wy*u[0]], [wx*u[1], wy*u[1]] ]).dot(F)
#       compute  R^T dF  - dF^T * R, anti-symetric matrix => diagonal zero and single value at the corner
        a = R[0,0]*dF[1,0] + R[1,0]*dF[1,1] - R[0,1]*dF[0,0] - R[1,1]*dF[0,1]
#       solve (R^T*dR) S + S (R^T*dR) =  R^T dF  - dF^T * R, same argument as above
        b = a / (S[0,0] + S[1,1]) 
#         deltaR = R*(R^T*dR)    
        dR = np.array([ [R[1,0]*b, R[0,0]*b], [R[1,1]*b, R[0,1]*b] ])
#         cofactor matrix of F, JF^-T
        cofF = np.array([ [F[1,1], -F[1,0]], [-F[0,1], F[0,0]] ])
        # cofF * dF
        cdF = cofF.dot(dF)
#         delta( cofF ) = J tr (F^-1 dF) F^-T - J F^-T * dF^T * F^-T
        dcofF = ((cdF[0,0] + cdF[1,1])*cofF - cofF.dot( cdF.T ))/J
#         (\partial Psi / \partial F partial dF) : delta F 
        A = 2*self.mu*(dF - dR) + self.lamda*( cofF* (cofF[0,0]*dF[0,0] + cofF[0,1]*dF[0,1] + cofF[1,0]*dF[1,0] + cofF[1,1]*dF[1,1]) + (J-1)*dcofF)
        
        return A
    
    def computeForceDiffParticle(self, dt):
        
        for k in range(self.nP):
            self.A[k] = np.zeros((2,2))
            #update particle vp
            x = self.pos[k][0]
            y = self.pos[k][1]
            i,j,u,v = self.grid.getCoord(x,y)            
            [Bx,dBx] = cubicSpline(u, self.grid.hx)
            [By,dBy] = cubicSpline(v, self.grid.hy)
            
            for i1 in range(0,4):
                for i2 in range(0,4):
                    idx = (i-1+i1,j-1+i2)
                    self.A[k] = self.A[k] + self.forceDiff(dt, self.grid.u[idx] , dBx[i1]*By[i2],  Bx[i1]*dBy[i2], self.F[k], self.R[k], self.S[k] )
    
    def computeForceDiffNode(self):
        for k in range(self.nP):
            x = self.pos[k][0]
            y = self.pos[k][1]
            i,j,u,v = self.grid.getCoord(x,y)            
            [Bx,dBx] = cubicSpline(u, self.grid.hx)
            [By,dBy] = cubicSpline(v, self.grid.hy)
            for i1 in range(0,4):
                for i2 in range(0,4):
                    idx = (i-1+i1,j-1+i2)
                    if (idx in self.grid.mass):
                        self.grid.df[idx] = self.grid.df[idx] - self.vol0[k]*self.A[k].dot( self.F[k].T.dot(np.array([dBx[i1]*By[i2], Bx[i1]*dBy[i2] ]) ))
                    else:
                        self.grid.df[idx] = - self.vol0[k]*self.A[k].dot( self.F[k].T.dot(np.array([dBx[i1]*By[i2], Bx[i1]*dBy[i2] ]) ))
        
'''
@summary: resolve collision between a pair of objects 
'''
def doCollision(obj1, obj2):
    for i, mm in enumerate(obj1.grid.mass):
        if (mm in obj2.grid.mass):
            n1 = obj1.grid.massdiff[mm]/np.linalg.norm(obj1.grid.massdiff[mm])
            n2 = obj2.grid.massdiff[mm]/np.linalg.norm(obj2.grid.massdiff[mm])
            n12 = (n1 - n2)/np.linalg.norm(n1-n2)
            if ( (obj1.grid.vf[mm] - obj2.grid.vf[mm]).dot(n12) > 0 ) :
                v_com = (obj1.grid.vf[mm]*obj1.grid.mass[mm] + obj2.grid.vf[mm]*obj2.grid.mass[mm])/(obj1.grid.mass[mm] + obj2.grid.mass[mm])
                obj1.grid.vf[mm] = obj1.grid.vf[mm] - ((obj1.grid.vf[mm] - v_com).dot(n12))*n12
                obj2.grid.vf[mm] = obj2.grid.vf[mm] - ((obj2.grid.vf[mm] - v_com).dot(n12))*n12
    return None

'''
@summary: driver routine 
'''
def animationLoop(objLst, collisionPairs, dt):
    #set up plot
    fig, ax = plt.subplots()
    plt.show(False)
    
    pplot = []
    for i, o in enumerate(objLst):
        if (i < 1):
            points, = ax.plot(o.pos[:,0],o.pos[:,1], 'bo')
            pplot.append(points)
        else:
            points, = ax.plot(o.pos[:,0],o.pos[:,1], 'ro')
            pplot.append(points)
    
    plt.draw()
    ax.grid(True)
    plt.axis([-2,2,-3,2])
    plt.gca().set_aspect('equal', adjustable='box')
    background = fig.canvas.copy_from_bbox(ax.bbox)
    #animation loop
    time_step = 1.0/24
    nStep = int(time_step/dt)
    while (True):
        # compute for grid first
        for i, o in enumerate(objLst):
            o.stepNode(dt)
        # do collision on grid
        for i, pair in enumerate(collisionPairs):
            doCollision(pair[0], pair[1])
        # advance particles to the new state
        for i, o in enumerate(objLst):
            o.stepParticle(dt)
        
        fig.canvas.restore_region(background)        
        for i, o in enumerate(objLst):
            pplot[i].set_data(o.pos[:,0],o.pos[:,1])
            ax.draw_artist(pplot[i])
        fig.canvas.blit(ax.bbox)    
    return None
    